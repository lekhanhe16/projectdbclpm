package view;

import controller.ElectricityPriceDAO;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import model.ElectricityPrice;

public class ConfigManageFrm extends JFrame implements ActionListener {

    private JPanel contentPane;
    private JLabel label;
    private JButton btnEdit;
    private JButton btnRefresh;
    private JScrollPane scrollPane;
    private JTable tblPrice;
    private ElectricityPriceDAO electricityPriceDAO;
    private ArrayList<ElectricityPrice> ePrices;

    public ConfigManageFrm() {
        initComponent();
        electricityPriceDAO = new ElectricityPriceDAO();
        ePrices = getTableData();
        fillTable(ePrices);

    }

    /**
     * Create the frame.
     */
    private void initComponent() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Quản lý cấu hình");
        setBounds(100, 100, 582, 428);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        Font labelFont = new Font("Arial", Font.BOLD, 24);
        Font buttonFont = new Font("Arial", Font.BOLD, 20);
        Font tableCellFont = new Font("Arial", Font.PLAIN, 17);
        Font tableHeaderFont = new Font("Arial", Font.BOLD, 17);

        label = new JLabel("Bảng cầu hình giá điện");
        label.setHorizontalAlignment(SwingConstants.CENTER);

        label.setFont(labelFont);
        label.setBounds(15, 16, 270, 36);
        contentPane.add(label);

        btnEdit = new JButton("Sửa");
        btnEdit.setBounds(430, 12, 115, 40);
        btnEdit.setFont(buttonFont);
        contentPane.add(btnEdit);

        btnRefresh = new JButton();
        btnRefresh.setBounds(515, 334, 30, 30);
        ImageIcon imageIcon = new ImageIcon("src\\refresh_icon.png");
        Image resizedImage = imageIcon.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT);
        ImageIcon icon = new ImageIcon(resizedImage);
        btnRefresh.setIcon(icon);
        contentPane.add(btnRefresh);

        scrollPane = new JScrollPane();
        scrollPane.setBounds(15, 68, 530, 260);
        contentPane.add(scrollPane);

        String[] cols = {"STT", "Từ (kWh)", "Tới (kWh)", "Giá (kVND/kWh)"};

        DefaultTableModel dtm = new DefaultTableModel(cols, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tblPrice = new JTable(dtm);
        tblPrice.setRowHeight(30);
        tblPrice.getTableHeader().setFont(tableHeaderFont);
        tblPrice.getTableHeader().setReorderingAllowed(false);
        tblPrice.setFont(tableCellFont);
        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setHorizontalAlignment(SwingConstants.RIGHT);

        TableColumnModel tcm = tblPrice.getColumnModel();
        tcm.getColumn(0).setPreferredWidth(10);
        for (int i = 0; i < tblPrice.getColumnCount(); i++) {
            tcm.getColumn(i).setResizable(false);
            tcm.getColumn(i).setCellRenderer(cellRenderer);
        }

        scrollPane.setViewportView(tblPrice);

        //add actionlistener
        btnEdit.addActionListener(this);
        btnRefresh.addActionListener(this);
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ConfigManageFrm frame = new ConfigManageFrm();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //thế nó hiện cái mô tê j lên vậy? chỉ là ko có message j j đo thooi à?
    //k  ro nua, bi loi tinh tiep, merge chua?
    //chưa, vao cho project 
    private ArrayList<ElectricityPrice> getTableData() {
        return electricityPriceDAO.getAll();
    }

    /**
     * Fill table
     */
    private void fillTable(ArrayList<ElectricityPrice> ePrices) {
        ElectricityPrice ePrice;
        DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
        dtm.setRowCount(0);
        for (int i = 0; i < ePrices.size(); i++) {
            ePrice = ePrices.get(i);
            Object[] object = new Object[4];
            object[0] = i + 1;
            object[1] = ePrice.getLowerBound();
            if (ePrice.getUpperBound() == -1) {
                object[2] = "--";
            } else {
                object[2] = ePrice.getUpperBound();
            }
            object[3] =  ePrice.getPrice();
            dtm.addRow(object);
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource().equals(btnEdit)) {
            EditConfigFrm editConfigFrm = new EditConfigFrm(ePrices, this, electricityPriceDAO);
            editConfigFrm.setVisible(true);
        }
        if (ae.getSource().equals(btnRefresh)){
            ePrices = getTableData();
            fillTable(ePrices);
        }
    }
}
