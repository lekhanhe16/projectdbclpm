package view;

import controller.ElectricityPriceDAO;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import model.ElectricityPrice;

public class EditConfigFrm extends JFrame implements ActionListener {

    private JPanel contentPane;
    private JButton btnConfirm;
    private JButton btnCancel;
    private JScrollPane scrollPane;
    private JTable tblPrice;
    private ConfigManageFrm configManageFrm;
    private ArrayList<ElectricityPrice> ePrices;
    private ElectricityPriceDAO electricityPriceDAO;
    private JButton btnAdd;
    private JButton btnDelete;
    
    //constructor
    public EditConfigFrm(ArrayList<ElectricityPrice> ePrices, ConfigManageFrm configManageFrm, ElectricityPriceDAO electricityPriceDAO) {
        initComponent();
        this.configManageFrm = configManageFrm;
        this.ePrices = ePrices;
        fillTable(this.ePrices);
        this.electricityPriceDAO = electricityPriceDAO;
    }

    /**
     * Create the frame.
     */
    private void initComponent() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("Sửa cấu hình");
        setBounds(100, 100, 582, 428);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        Font labelFont = new Font("Arial", Font.BOLD, 24);
        Font buttonFont = new Font("Arial", Font.BOLD, 20);
        Font tableCellFont = new Font("Arial", Font.PLAIN, 17);
        Font tableHeaderFont = new Font("Arial", Font.BOLD, 17);

        btnConfirm = new JButton("Xác nhận");
        btnConfirm.setBounds(275, 316, 130, 40);
        btnConfirm.setFont(buttonFont);
        contentPane.add(btnConfirm);

        btnCancel = new JButton("Hủy");
        btnCancel.setBounds(420, 316, 125, 40);
        btnCancel.setFont(buttonFont);
        contentPane.add(btnCancel);

        btnAdd = new JButton("Thêm");
        btnAdd.setBounds(15, 316, 115, 40);
        btnAdd.setFont(buttonFont);
        contentPane.add(btnAdd);

        btnDelete = new JButton("Xóa");
        btnDelete.setBounds(145, 316, 115, 40);
        btnDelete.setFont(buttonFont);
        contentPane.add(btnDelete);

        scrollPane = new JScrollPane();
        scrollPane.setBounds(15, 16, 530, 288);
        contentPane.add(scrollPane);

        String[] cols = {"STT", "Từ (kWh)", "Tới (kWh)", "Giá (kVND/kWh)"};

        DefaultTableModel dtm = new DefaultTableModel(cols, 8) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return true;
            }
        };

        tblPrice = new JTable(dtm);
        tblPrice.setRowHeight(30);
        tblPrice.getTableHeader().setFont(tableHeaderFont);
        tblPrice.getTableHeader().setReorderingAllowed(false);
        tblPrice.setFont(tableCellFont);
        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setHorizontalAlignment(SwingConstants.RIGHT);

        TableColumnModel tcm = tblPrice.getColumnModel();
        tcm.getColumn(0).setPreferredWidth(10);
        for (int i = 0; i < tblPrice.getColumnCount(); i++) {
            tcm.getColumn(i).setResizable(false);
            tcm.getColumn(i).setCellRenderer(cellRenderer);

        }

        scrollPane.setViewportView(tblPrice);

        //add actionlistener
        btnConfirm.addActionListener(this);
        btnCancel.addActionListener(this);
        btnAdd.addActionListener(this);
        btnDelete.addActionListener(this);
    }
    //process button clicked
    @Override
    public void actionPerformed(ActionEvent ae) {
        DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
        //confirm button clicked
        if (ae.getSource().equals(btnConfirm)) {
            ArrayList<ElectricityPrice> ePrices = getDataFromTable();
            if (ePrices != null) {
                ePrices = arrangeTableData(ePrices);
                fillTable(ePrices);
                int result = checkConfigValid(ePrices);
                if (result == -1) {
                    /**
                     * cầu hình chính xác
                     */
                    System.out.println("cấu hình chuẩn");
                    /** 
                     * hiện thông báo chọn yes no 
                     */
                     int choice = JOptionPane.showConfirmDialog(this, "Thay đổi cấu hình đã được chấp nhận, Bạn có muốn lưu cấu hình?");
                     if (choice == JOptionPane.YES_OPTION){
                        //chọn YES -> update db
                         electricityPriceDAO.deleteAll();
                         electricityPriceDAO.addValues(ePrices);
                     }
                     else {// chọn NO -> ko làm gì cả
                         
                     }
                     
                } else {
                    /**
                     * cấu hình ko chính xác
                     * báo dòng bị lỗi
                     */
                    JOptionPane.showMessageDialog(this, "Cấu hình bị lỗi ở dòng " + (result + 1));
                }

            }
        }
        //cancel button clicked
        if (ae.getSource().equals(btnCancel)) {
            this.dispose();

        }
        //add button clicked
        if (ae.getSource().equals(btnAdd)) {
            int index = tblPrice.getSelectedRow();
            if (index >= 0) {
                dtm.insertRow(index + 1, new Object[]{null, null, null, null});
            } else {
                dtm.addRow(new Object[]{null, null, null, null});
            }
            updateSTT(dtm);
        }
        //delete button clicked
        if (ae.getSource().equals(btnDelete)) {
            int index = tblPrice.getSelectedRow();
            if (index >= 0) {
                dtm.removeRow(index);
            }
            updateSTT(dtm);

        }
    }
    // fill table 
    private void fillTable(ArrayList<ElectricityPrice> ePrices) {
        ElectricityPrice ePrice;
        DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
        dtm.setRowCount(0);
        for (int i = 0; i < ePrices.size(); i++) {
            ePrice = ePrices.get(i);
            Object[] object = new Object[4];
            object[0] = i + 1;
            object[1] = ePrice.getLowerBound();
            if (ePrice.getUpperBound() == -1) {
                object[2] = "--";
            } else {
                object[2] = ePrice.getUpperBound();
            }
            object[3] = ePrice.getPrice();
            dtm.addRow(object);
        }
    }
    //update stt column in the table
    private void updateSTT(DefaultTableModel dtm) {
        for (int i = 0; i < dtm.getRowCount(); i++) {
            dtm.setValueAt(i + 1, i, 0);
        }
    }
    //get input data from the table 
    //to make new set of configuration
    private ArrayList<ElectricityPrice> getDataFromTable() {
        ArrayList<ElectricityPrice> ePrices = new ArrayList<ElectricityPrice>();
        DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
        for (int i = 0; i < dtm.getRowCount(); i++) {
            Object[] objects = new Object[3];
            objects[0] = dtm.getValueAt(i, 1).toString();
            objects[1] = dtm.getValueAt(i, 2).toString();
            objects[2] = dtm.getValueAt(i, 3).toString();
            ElectricityPrice ePrice = checkData(objects);
            if (ePrice == null) {
                return null;
            } else {
                ePrices.add(ePrice);
            }
        }
        return ePrices;
    }
    // check input validation 
    private ElectricityPrice checkData(Object[] objects) {
        ElectricityPrice ePrice = new ElectricityPrice();
        int lowerBound;
        int upperBound;
        double price;

        /**
         * check if inputs are numbers;
         */
        try {
            lowerBound = Integer.parseInt((String) objects[0]);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Giới hạn dưới phải là 1 số nguyên");
            return null;
        }

        try {
            if (objects[1].equals("--")) {
                upperBound = -1;
            } else {
                upperBound = Integer.parseInt((String) objects[1]);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Giới hạn trên phải là 1 số nguyên");
            return null;
        }

        try {
            price = Double.parseDouble((String) objects[2]);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Giá tiền phải là 1 số thực");
            return null;
        }

        /**
         * check for validation
         */
        if (lowerBound < 0) {
            JOptionPane.showMessageDialog(this, "Giới hạn dưới phải lớn hơn hoặc bằng 0");
            return null;
        }

        if (upperBound <= 0 && !objects[1].equals("--")) {
            JOptionPane.showMessageDialog(this, "Giới hạn trên phải lớn hơn 0");
            return null;
        }

        if (price <= 0) {
            JOptionPane.showMessageDialog(this, "Giá tiền phải lớn hơn 0");
            return null;
        }

        if (lowerBound >= upperBound && !objects[1].equals("--")) {
            JOptionPane.showMessageDialog(this, "Giới hạn dưới phải nhỏ hơn giới hạn trên");
            return null;
        }

        ePrice.setLowerBound(lowerBound);
        ePrice.setUpperBound(upperBound);
        ePrice.setPrice(price);
        return ePrice;
    }
    //sort table
    private ArrayList<ElectricityPrice> arrangeTableData(ArrayList<ElectricityPrice> ePrices) {
        ElectricityPrice temp;
        ElectricityPrice ePriceI;
        ElectricityPrice ePriceJ;

        for (int i = 0; i < ePrices.size() - 1; i++) {
            ePriceI = ePrices.get(i);
            for (int j = i + 1; j < ePrices.size(); j++) {
                ePriceJ = ePrices.get(j);
                if (ePriceI.getLowerBound() > ePriceJ.getLowerBound() && ePriceI.getUpperBound() > ePriceJ.getUpperBound()) {
                    temp = ePriceI;
                    ePriceI = ePriceJ;
                    ePriceJ = temp;
                }
                ePrices.set(i, ePriceI);
                ePrices.set(j, ePriceJ);
            }
        }
        return ePrices;
    }
    //check config valid
    private int checkConfigValid(ArrayList<ElectricityPrice> ePrices) {
        // lowerbound of first price 
        // must equal 0
        if (ePrices.get(0).getLowerBound() != 0) { 
            return 0;//return the error line
        }
        //check for if the upperbound of 
        //the previous price is equal to lowerbound 
        //of the next price +1
        for (int i = 1; i < ePrices.size(); i++) {
            if (ePrices.get(i).getLowerBound() - 1 != ePrices.get(i - 1).getUpperBound()) {
                return i;//return to error line
            }
        }
        //check if the last price has upperbound = -1 
        //(means infinite)
        if (ePrices.get(ePrices.size() - 1).getUpperBound() != -1) {
            return ePrices.size() - 1;//return the error line
        }
        return -1;//if no error 
    }

}
