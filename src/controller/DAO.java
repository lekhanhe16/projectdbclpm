/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ASUS
 */
public class DAO {
    //fixed by Quang, su dung singleton pattern
    private static DAO instance;
    private Connection connection;
    private String url = "jdbc:mysql://localhost:3306/electricity" + "?useUnicode=true&characterEncoding=UTF-8";
    private String username = "root";
    private String password = "";
    private String driver = "com.mysql.jdbc.Driver";
    private DAO(){ 
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, username, password);
            System.out.println("DB connection: success.");
        } catch (Exception e) {
            System.out.println("DB connecttion: fail.");
            e.printStackTrace();
        }
       
    }
    public Connection getConnection(){
        return connection;
    }
    
    public static DAO getInstance() throws SQLException{
        if(instance==null){
            instance = new DAO();
        }
        else if(instance.getConnection().isClosed()){
            instance = new DAO();
        }
        return instance;
    }

}
