/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Address;
import model.Customer;
import model.FullName;

/**
 *
 * @author Ryan
 */
public class CustomerDAO {

    Connection conn;

    public CustomerDAO() {
        try {
            conn = DAO.getInstance().getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Customer> getListCustomer() {
        ArrayList<Customer> listCustomer = new ArrayList<>();
        try {
            String sql = "SELECT * FROM customer, fullname, account, address WHERE "
                    + "customer.fullnameId = fullname.id AND customer.accountId = account.id AND customer.addressId = address.id";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Customer customer = new Customer();
                customer.setCustomerID(rs.getString("customerID"));
                customer.setFullName(new FullName(rs.getString("firstName"), rs.getString("middleName"), rs.getString("lastName")));
                customer.setDateOfBirth(rs.getDate("dateOfBirth"));
                customer.setAddress(new Address(rs.getString("addressLine"), rs.getString("district"), rs.getString("city"), rs.getString("country")));
                customer.setAccount(new Account(rs.getString("username"), rs.getString("password")));
                customer.setEmail(rs.getString("email"));
                customer.setTel(rs.getString("tel"));
                listCustomer.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listCustomer;
    }

    public Customer getByID(String cusID) {
        Customer customer = null;
        try {
            String sql = "SELECT * FROM customer, fullname, account, address WHERE "
                    + "customer.fullnameId = fullname.id AND customer.accountId = account.id AND customer.addressId = address.id and customer.customerID = ?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, cusID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                customer = new Customer();
                customer.setCustomerID(rs.getString("customerID"));
                customer.setFullName(new FullName(rs.getString("firstName"), rs.getString("middleName"), rs.getString("lastName")));
                customer.setDateOfBirth(rs.getDate("dateOfBirth"));
                customer.setAddress(new Address(rs.getString("addressLine"), rs.getString("district"), rs.getString("city"), rs.getString("country")));
                customer.setAccount(new Account(rs.getString("username"), rs.getString("password")));
                customer.setEmail(rs.getString("email"));
                customer.setTel(rs.getString("tel"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return customer;
    }
}
